<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Back\Master\TaskController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Credential\CredentialController;
use App\Http\Controllers\Credential\ProfileController;
use App\Http\Controllers\Back\Master\DivisiController;
use App\Http\Controllers\Back\Master\CheckpointController;
use App\Http\Controllers\Back\Ktp\KtpController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::get('helper/download', [\App\Http\Controllers\Back\HelperController::class, 'download'])->name('helper.download');

Route::group(['middleware' => ['auth']], function () {

        Route::resource('users', CredentialController::class)->middleware('role:administrator');
        Route::resource('profile', ProfileController::class);


        Route::resource('gudang', \App\Http\Controllers\Back\Master\GudangController::class);
        Route::resource('people', \App\Http\Controllers\Back\Master\PeopleController::class);
        Route::resource('kategori', \App\Http\Controllers\Back\Master\KategoriController::class);
        Route::resource('item', \App\Http\Controllers\Back\Master\ItemController::class);
        Route::resource('satuan', \App\Http\Controllers\Back\Master\SatuanController::class);
        Route::get('asset-user', [\App\Http\Controllers\Back\UserItemController::class, 'index'])
            ->name('asset-user.index');
        Route::get('asset-user-assign', [\App\Http\Controllers\Back\UserItemController::class, 'assign'])
            ->name('asset-user-assign.index');
        Route::delete('asset-user-unassign', [\App\Http\Controllers\Back\UserItemController::class, 'unAssignItem'])
            ->name('asset-user-unassign.index');
        Route::post('asset-user/store', [\App\Http\Controllers\Back\UserItemController::class, 'assignItem'])
            ->name('asset-user.store');
    });

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
