<table border="1">
    <thead>
    <tr>
        <th>No</th>
        <th>Tugas</th>
        <th>PIC</th>
        <th>Tanggal Mulai</th>
        <th>Tanggal Selesai</th>
        <th>Catatan</th>
        <th>Status</th>
        <th>Checkpoint</th>
        <th>Lampiran</th>
    </tr>
    </thead>
    <tbody>
    @php($zz=1)
    @foreach($data as $datum)
    <tr>
        <td>{{$zz++}}</td>
        <td>{{$datum->task}}</td>
        <td>{{$datum->users->name}}</td>
        <td>{{$datum->start_date}}</td>
        <td>{{$datum->due_date}}</td>
        <td>{{$datum->note}}</td>
        <td>{{$datum->codes->code_nm}}</td>
        <td>
            <ol>
            @foreach($datum->check as $checkpoint)
                <li>
                    ({{$checkpoint->created_at}}) {{$checkpoint->note}}
                </li>
            @endforeach
            </ol>
        </td>
        <td>
            @if($datum->file != '')
                {{route('helper.download',['file' => $datum->file, 'file_name' => $datum->file_name])}}
            @endif
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
