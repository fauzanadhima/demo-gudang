
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" >
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>GUDANG</title>

    <!-- Global stylesheets -->
        @include('layouts.css')

    <!-- /global stylesheets -->

    <!-- Core JS files -->
        @yield('style')

    <!-- /core JS files -->

    <!-- Theme JS files -->

    <!-- /theme JS files -->

</head>

<body>

<!-- Main navbar -->
@include('layouts.navbar')
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    @include('layouts.sidebar')
    <!-- /main sidebar -->


    <!-- Secondary sidebar -->

    <!-- /secondary sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
{{--        <div class="page-header page-header-light">--}}
{{--            <div class="page-header-content header-elements-md-inline">--}}
{{--                <div class="page-title d-flex">--}}
{{--                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Sidebars</span> - Components</h4>--}}
{{--                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
        @yield('content')

            <!-- Sidebars overview -->


        </div>
        <!-- /content area -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</span>

{{--                <ul class="navbar-nav ml-lg-auto">--}}
{{--                    <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>--}}
{{--                    <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>--}}
{{--                    <li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>--}}
{{--                </ul>--}}
            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
@include('layouts.js')
<script>
    $(document).ready(function () {

        $(".alert-success").delay(4000).slideUp(200, function() {
            $(this).alert('close');
        });

        var url = window.location;
        $('a[href="'+url+'"]').parent('li').addClass('active');
        // let asu =   $('li .nav-item  a[href="' + url + '"]').addClass('active');
        // console.log(asu);
        $('li .nav-item  a[href="' + url + '"]').parents().closest('li').addClass('open active');
        $(document).on('click', '.delete-data-table', function(a){
            a.preventDefault();
            swal({
                title: 'Are you sure?',
                text: "Do you realy want to delete this records? This process cannot be undone.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete!'
            }).then((result) => {
                if (result.value) {
                    a.preventDefault();
                    var url = $(this).attr('href');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: url,
                        method: 'delete',
                        success: function () {
                            swal(
                                'Deleted!',
                                'data has been deleted.',
                                'success'
                            )
                            table1.ajax.reload();
                            if(typeof table2){
                                table2.ajax.reload();
                            }
                        }
                    })
                }
            })
        });
    })
</script>
@stack('js')
</body>
</html>
