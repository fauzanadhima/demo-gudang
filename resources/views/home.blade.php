@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-xl-12">

                            <!-- Progress counters -->
                            <div class="row">
                                <div class="col-sm-6">

                                    <!-- Available hours -->
                                    <div class="card text-center">
                                        <div class="card-body">

                                            <!-- Progress counter -->
                                            <div class="svg-center position-relative" id="hours-available-progress"><svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(238, 238, 238);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); stroke: rgb(240, 98, 146);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); fill-opacity: 1;"></path></g></svg><h2 class="pt-1 mt-2 mb-1">
                                                {{$nilaiAwal}}</h2><i class="icon-grid5  text-pink-400 counter-icon" style="top: 22px"></i><div>Jumlah Aset</div><div class="font-size-sm text-muted mb-3"></div></div>
                                            <!-- /progress counter -->


                                            <!-- Bars -->
                                            <div class="svg-center position-relative" id="hours-available-bars"><svg width="114.484375" height="40"><g width="114.484375"><rect class="d3-random-bars" width="3.2979038065843618" x="1.4133873456790123" height="36" y="4" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="6.124678497942387" height="26" y="14" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="10.835969650205762" height="28" y="12" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="15.547260802469136" height="30" y="10" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="20.25855195473251" height="24" y="16" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="24.969843106995885" height="26" y="14" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="29.68113425925926" height="20" y="20" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="34.39242541152264" height="32" y="8" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="39.10371656378601" height="34" y="6" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="43.81500771604938" height="32" y="8" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="48.52629886831276" height="26" y="14" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="53.237590020576135" height="24" y="16" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="57.948881172839506" height="20" y="20" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="62.66017232510288" height="28" y="12" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="67.37146347736626" height="32" y="8" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="72.08275462962963" height="32" y="8" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="76.794045781893" height="36" y="4" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="81.50533693415638" height="32" y="8" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="86.21662808641975" height="22" y="18" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="90.92791923868313" height="38" y="2" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="95.6392103909465" height="34" y="6" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="100.35050154320987" height="30" y="10" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="105.06179269547326" height="40" y="0" style="fill: rgb(236, 64, 122);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="109.77308384773663" height="34" y="6" style="fill: rgb(236, 64, 122);"></rect></g></svg></div>
                                            <!-- /bars -->

                                        </div>
                                    </div>
                                    <!-- /available hours -->

                                </div>

                                <div class="col-sm-6">

                                    <!-- Productivity goal -->
                                    <div class="card text-center">
                                        <div class="card-body">

                                            <!-- Progress counter -->
                                            <div class="svg-center position-relative" id="goal-progress"><svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(238, 238, 238);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.3834279937087,-16.179613079472855L-32.573773888776664,-15.328054496342704A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(92, 107, 192); stroke: rgb(92, 107, 192);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.3834279937087,-16.179613079472855L-32.573773888776664,-15.328054496342704A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(92, 107, 192); fill-opacity: 1;"></path></g></svg><h2 class="pt-1 mt-2 mb-1">
                                                {{$nilaiAkhir}}</h2><i class="icon-googleplus5  text-indigo-400 counter-icon" style="top: 22px"></i><div>Jumlah Kuantitas</div><div class="font-size-sm text-muted mb-3"></div></div>
                                            <!-- /progress counter -->

                                            <!-- Bars -->
                                            <div class="svg-center position-relative" id="goal-bars"><svg width="114.484375" height="40"><g width="114.484375"><rect class="d3-random-bars" width="3.2979038065843618" x="1.4133873456790123" height="29.473684210526315" y="10.526315789473685" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="6.124678497942387" height="31.578947368421055" y="8.421052631578945" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="10.835969650205762" height="23.157894736842106" y="16.842105263157894" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="15.547260802469136" height="33.68421052631579" y="6.315789473684212" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="20.25855195473251" height="31.578947368421055" y="8.421052631578945" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="24.969843106995885" height="33.68421052631579" y="6.315789473684212" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="29.68113425925926" height="37.89473684210526" y="2.10526315789474" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="34.39242541152264" height="29.473684210526315" y="10.526315789473685" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="39.10371656378601" height="23.157894736842106" y="16.842105263157894" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="43.81500771604938" height="23.157894736842106" y="16.842105263157894" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="48.52629886831276" height="29.473684210526315" y="10.526315789473685" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="53.237590020576135" height="29.473684210526315" y="10.526315789473685" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="57.948881172839506" height="35.78947368421053" y="4.210526315789473" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="62.66017232510288" height="27.36842105263158" y="12.631578947368421" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="67.37146347736626" height="29.473684210526315" y="10.526315789473685" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="72.08275462962963" height="37.89473684210526" y="2.10526315789474" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="76.794045781893" height="23.157894736842106" y="16.842105263157894" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="81.50533693415638" height="37.89473684210526" y="2.10526315789474" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="86.21662808641975" height="37.89473684210526" y="2.10526315789474" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="90.92791923868313" height="40" y="0" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="95.6392103909465" height="25.263157894736842" y="14.736842105263158" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="100.35050154320987" height="21.052631578947366" y="18.947368421052634" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="105.06179269547326" height="31.578947368421055" y="8.421052631578945" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="3.2979038065843618" x="109.77308384773663" height="23.157894736842106" y="16.842105263157894" style="fill: rgb(92, 107, 192);"></rect></g></svg></div>
                                            <!-- /bars -->

                                        </div>
                                    </div>
                                    <!-- /productivity goal -->

                                </div>
                            </div>
                            <!-- /progress counters -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
