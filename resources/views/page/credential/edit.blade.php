@extends('layouts.app')

@section('content')

    <section id="description" class="card">
        <div class="card-header">
            <h4 class="card-title">Edit User</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                {!! Form::model($data,['route' => ['users.update', $data->id],'files' => true, 'method' => 'put', 'id' => 'form-task'])!!}
                <div class="form-body">
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::email('email' ,null, ['id' => 'email', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('alamat', 'Alamat') !!}
                                {!! Form::text('alamat', null, ['id' => 'alamat', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('telepon', 'Phone') !!}
                                {!! Form::text('telepon', null, ['id' => 'telepon', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('role', 'Role') !!}
                                {!! Form::select('role', $pilihan,null, ['id' => 'role', 'class' => 'form-control', 'placeholder' => 'Choose Role']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('password', 'Password') !!}
                                {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Password']) !!}
                            </div>
                        </div>
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('password_confirmation', 'Password Confirmation') !!}
                                {!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => 'Password Confirmation']) !!}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions">
                    <a href="{{route('users.index')}}" class="btn btn-warning mr-1" onclick="return confirm('Apa anda yakin?')">
                        <i class="feather icon-corner-down-left"></i> Back
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Submit
                    </button>
                </div>
                {!! Form::close()  !!}
            </div>
        </div>
    </section>
@endsection

@push('js')
        {!! JsValidator::formRequest('App\Http\Requests\Credential\UpdateRequest') !!}
@endpush
