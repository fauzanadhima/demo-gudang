@extends('layouts.app')

@section('content')
    <section id="description" class="card">
        <div class="card-header">
            <h4 class="card-title">Gudang</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                {!! Form::open(['route' => 'gudang.store','files' => true, 'id' => 'form-task'])!!}

                <div class="form-body">
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('alamat', 'Alamat') !!}
                                {!! Form::textarea('alamat', null, ['id' => 'alamat', 'rows' => 3 , 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions">
                    <a href="{{route('gudang.index')}}" class="btn btn-warning mr-1" onclick="return confirm('Apa anda yakin?')">
                        <i class="feather icon-corner-down-left"></i> Back
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Submit
                    </button>
                </div>
                {!! Form::close()  !!}
            </div>
        </div>
    </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Master\Gudang\CreateRequest') !!}
@endpush

