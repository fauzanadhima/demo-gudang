@extends('layouts.app')

@section('content')
    <div id="description" class="card">
        <div class="card-header">
            <h4 class="card-title">Item</h4>
            <div class="col md-3">
                <a href="{{route('item.create')}}" class="btn btn-primary float-right" >Create</a>
            </div>
        </div>
        <div class="card-content">
            <div class="card-body">
                @if(Session::has('keterangan'))
                    <div class="alert alert-success alert-dismissible mb-2" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{Session::get('keterangan')}}
                    </div>
                @endif
                <table class="table table-striped table-bordered dt-responsive" id="table1" width="100%">
                    <thead>
                    <th width="30px">No</th>
                    <th>Name</th>
                    <th>Lokasi</th>
                    <th>Kategori</th>
                    <th>Satuan</th>
                    <th>Tanggal Pengadaan</th>
                    <th>Nilai Awal</th>
                    <th>Depresiasi</th>
                    <th>Nilai Akhir</th>
                    <th>Kode</th>
                    <th class="text-center">QR</th>
                    <th width="140px">Action</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        const table1 =  $('#table1').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive" :true,
            "ajax": window.location.href,
            "columns": [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false, className: "text-right"},
                { data: "name"},
                { data: "gudang.name", name: "gudang.name"},
                { data: "kategori.name", name: "kategori.name"},
                { data: "satuan.name", name: "satuan.name", defaultContent: "-"},
                { data: "tanggal_pengadaan"},
                { data: "nilai_barang"},
                { data: "depresiasi", orderable: false, searchable: false, className: "text-right", className: 'text-center'},
                { data: "nilai_akhir", orderable: false, searchable: false, className: "text-right", className: 'text-center'},
                { data: "kode"},
                { data: "qr", orderable: false, searchable: false, className: "text-right", className: 'text-center'},
                { data: "action",  className: "text-center"},
            ]
        });
    </script>
@endpush

