@extends('layouts.app')

@section('content')
    <div id="description" class="card">
        <div class="card-header">
            <h4 class="card-title">Details Item</h4>
        </div>
        <div class="card-content">
            <table class="table table-striped">
                <tr>
                    <td colspan="2" class="bg-blue-grey bg-lighten-3">Informasi</td>
                </tr>
                <tr>
                    <td width="20%">Nama</td>
                    <td>: {{$data->name}}</td>
                </tr>
                <tr>
                    <td width="20%">Lokasi</td>
                    <td>: {{$data->gudang->name}}</td>
                </tr>
                <tr>
                    <td width="20%">Kategori</td>
                    <td>: {{ucwords($data->kategori->name)}}</td>
                </tr>
                <td width="20%">Satuan</td>
                <td>: {{$data->satuan->name}}</td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Pengadaan</td>
                    <td>: {{date('d F Y', strtotime($data->tanggal_pengadaan))}}</td>
                </tr>
                <tr>
                    <td width="20%">Nilai Awal</td>
                    <td>: {{$data->nilai_barang}}</td>
                </tr>
                <tr>
                    <td width="20%">Kode</td>
                    <td>: {{$data->kode}}</td>
                </tr>
                <tr>
                    <td width="20%">QR</td>
                    <td>: {{QrCode::generate($data->kode)}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection


