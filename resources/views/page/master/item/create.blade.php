@extends('layouts.app')

@section('content')
    <section id="description" class="card">
        <div class="card-header">
            <h4 class="card-title">Tambah Item</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                {!! Form::open(['route' => 'item.store','files' => true, 'id' => 'form-task'])!!}

                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('kode', 'Kode') !!}
                                {!! Form::text('kode', null, ['id' => 'kode' , 'rows' => 3 ,'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) !!}
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('kategori_id', 'Kategori') !!}
                                {!! Form::select('kategori_id', $kategori,null, ['id' => 'kategori_id', 'class' => 'form-control', 'placeholder' => 'Pilih Satu']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('gudang_id', 'Lokasi') !!}
                                {!! Form::select('gudang_id',$gudang ,null, ['id' => 'gudang_id' , 'class' => 'form-control', 'placeholder' => 'Pilih Satu']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('kategori_id', 'Tanggal Pengadaan') !!}
                                {!! Form::date('tanggal_pengadaan', null, ['id' => 'kategori_id', 'class' => 'form-control', 'placeholder' => 'Pilih Satu']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('satuan_id', 'Satuan') !!}
                                {!! Form::select('satuan_id',$satuan ,null, ['id' => 'satuan_id' , 'class' => 'form-control', 'placeholder' => 'Pilih Satu']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col md-6">
                            <div class="form-group">
                                {!! Form::label('gudang_id', 'Nilai Asset') !!}
                                {!! Form::number('nilai_barang' ,null, ['id' => 'gudang_id' , 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('kategori_id', 'Nilai Residu') !!}
                                {!! Form::number('nilai_residu', null, ['id' => 'kategori_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('gudang_id', 'Estimasi Usia Pakai') !!}
                                {!! Form::number('estimasi_pakai' ,null, ['id' => 'gudang_id' , 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions">
                    <a href="{{route('item.index')}}" class="btn btn-warning mr-1" onclick="return confirm('Apa anda yakin?')">
                        <i class="feather icon-corner-down-left"></i> Back
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Submit
                    </button>
                </div>
                {!! Form::close()  !!}
            </div>
        </div>
    </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Master\Item\CreateRequest') !!}
@endpush

