<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Code;

class CodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            ['code_cd' => 'STATUS_01', 'code_nm' => 'KAWIN','code_group' => 'STATUS', 'code_value' => '' ],
            ['code_cd' => 'STATUS_02', 'code_nm' => 'BELUM KAWIN','code_group' => 'STATUS', 'code_value' => '' ],
            ['code_cd' => 'STATUS_03', 'code_nm' => 'JANDA','code_group' => 'STATUS', 'code_value' => '' ],
            ['code_cd' => 'STATUS_04', 'code_nm' => 'DUDA','code_group' => 'STATUS', 'code_value' => '' ],
            ['code_cd' => 'GENDER_01', 'code_nm' => 'LAKI-LAKI','code_group' => 'GENDER', 'code_value' => '' ],
            ['code_cd' => 'GENDER_02', 'code_nm' => 'PEREMPUAN','code_group' => 'GENDER', 'code_value' => '' ],
            ['code_cd' => 'KEWARGANEGARAAN_01', 'code_nm' => 'WNI','code_group' => 'KEWARGANEGARAAN', 'code_value' => '' ],
            ['code_cd' => 'KEWARGANEGARAAN_02', 'code_nm' => 'WNA','code_group' => 'KEWARGANEGARAAN', 'code_value' => '' ],
            ['code_cd' => 'STATUS_KK_01', 'code_nm' => 'SUAMI','code_group' => 'STATUS_KK', 'code_value' => '' ],
            ['code_cd' => 'STATUS_KK_02', 'code_nm' => 'ISTRI','code_group' => 'STATUS_KK', 'code_value' => '' ],
            ['code_cd' => 'STATUS_KK_03', 'code_nm' => 'ANAK','code_group' => 'STATUS_KK', 'code_value' => '' ],




        ];
        foreach ($data as $datum) {
            Code::create($datum);
        }
        $code = Code::create(["code_cd" =>"RELIGION_CD_01" ,"code_nm"=>"ISLAM" ,"code_group"=>"RELIGION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"RELIGION_CD_02" ,"code_nm"=>"KATOLIK" ,"code_group"=>"RELIGION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"RELIGION_CD_03" ,"code_nm"=>"PROTESTAN" ,"code_group"=>"RELIGION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"RELIGION_CD_04" ,"code_nm"=>"HINDU" ,"code_group"=>"RELIGION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"RELIGION_CD_05" ,"code_nm"=>"BUDHA" ,"code_group"=>"RELIGION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"RELIGION_CD_06" ,"code_nm"=>"KONG HU CU" ,"code_group"=>"RELIGION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"RELIGION_CD_07" ,"code_nm"=>"ALIRAN KEPERCAYAAN" ,"code_group"=>"RELIGION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_01" ,"code_nm"=>"PNS" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_02" ,"code_nm"=>"TNI" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_03" ,"code_nm"=>"POLRI" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_04" ,"code_nm"=>"GURU" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_05" ,"code_nm"=>"DOSEN" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_06" ,"code_nm"=>"PELAJAR" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_07" ,"code_nm"=>"MAHASISWA" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_08" ,"code_nm"=>"PETANI" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_09" ,"code_nm"=>"NELAYAN" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_10" ,"code_nm"=>"BURUH" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_11" ,"code_nm"=>"SWASTA" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_12" ,"code_nm"=>"WIRASWASTA" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_13" ,"code_nm"=>"SEKTOR INFORMAL" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_14" ,"code_nm"=>"IBU RUMAH TANGGA" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_98" ,"code_nm"=>"PENSIUN" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"OCCUPATION_CD_99" ,"code_nm"=>"LAIN-LAIN" ,"code_group"=>"OCCUPATION_CD" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"BLOOD_TP_A" ,"code_nm"=>"A" ,"code_group"=>"BLOOD_TP" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"BLOOD_TP_AB" ,"code_nm"=>"AB" ,"code_group"=>"BLOOD_TP" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"BLOOD_TP_B" ,"code_nm"=>"B" ,"code_group"=>"BLOOD_TP" , "code_value"=>null]);
        $code = Code::create(["code_cd" =>"BLOOD_TP_O" ,"code_nm"=>"O" ,"code_group"=>"BLOOD_TP" , "code_value"=>null]);
    }

}
