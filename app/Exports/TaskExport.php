<?php

namespace App\Exports;

use App\Models\Task;
use App\Models\User;
use http\Env\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TaskExport implements FromView
{
    public $r;
    public function __construct($request)
    {
        $this->r = $request;
    }

    public function view(): View
    {
//        dd($this->r->tanggal_mulai);
        $a = Task::with(['check','users','codes','check'])
            ->whereRaw(
                "(created_at >= ? AND created_at <= ?)",
                [$this->r->tanggal_mulai." 00:00:00", $this->r->tanggal_selesai." 23:59:59"]
            )
            ->get();//whereBetween('start_date',[$this->r->tanggal_mulai, $this->r->tanggal_selesai] )->get();
        return view('exports.task', ['data' => $a]);
    }
}
