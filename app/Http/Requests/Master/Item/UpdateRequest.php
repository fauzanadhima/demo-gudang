<?php

namespace App\Http\Requests\Master\Item;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode' => 'required',
            'name' => 'required',
            'kategori_id' => 'required',
            'gudang_id' => 'required',
            'satuan_id' => 'required',
            'nilai_barang' => 'required',
            'nilai_residu' => 'required',
            'estimasi_pakai' => 'required',
        ];
    }
}
