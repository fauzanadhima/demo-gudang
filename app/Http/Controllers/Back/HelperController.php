<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;

class HelperController extends Controller
{
    public function download(Request $request)
    {

        return Storage::download($request->file, $request->file_name??'Lampiran.'.substr($request->file,-3));
    }
}
