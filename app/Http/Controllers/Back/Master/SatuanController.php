<?php

namespace App\Http\Controllers\Back\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Master\Kategori\CreateRequest;
use App\Http\Requests\Master\Kategori\UpdateRequest;
use App\Models\Satuan;
use Illuminate\Http\Request;
use DataTables;
use Session;

class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Satuan::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($a) {
                    return '<a href="' . route('satuan.edit', $a->id) . '" class="primary edit mr-1"><i class="fa fa-pencil mr-2"></i></a>
                            <a href="' . route('satuan.destroy', $a->id) . '" class="danger delete delete-data-table mr-1" ><i class="fa fa-trash mr-2"></i></a>';
                })
                ->make(true);
        }

        return view('page.master.satuan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.master.satuan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $data = Satuan::create(
            [
                'name' => $request->name,
            ]
        );

        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('satuan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Satuan::find($id);

        return view('page.master.satuan.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = Satuan::find($id)
            ->update($request->except('_token', '_method', 'proengsoft_jsvalidation'));
        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('satuan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Satuan::destroy($id);

        return $data;
    }
}
