<?php

namespace App\Http\Controllers\Back\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Master\Gudang\CreateRequest;
use App\Http\Requests\Master\Gudang\UpdateRequest;
use App\Models\Gudang;
use Illuminate\Http\Request;
use DataTables;
use Session;

class GudangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Gudang::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($a) {
                    return '<a href="' . route('gudang.edit', $a->id) . '" class="primary edit mr-1"><i class="fa fa-pencil mr-2"></i></a>
                            <a href="' . route('gudang.destroy', $a->id) . '" class="danger delete delete-data-table mr-1" ><i class="fa fa-trash mr-2"></i></a>';
                })
                ->make(true);
        }

        return view('page.master.gudang.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.master.gudang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $data = Gudang::create(
            [
                'name' => $request->name,
                'alamat' => $request->alamat,
            ]
        );

        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('gudang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Gudang::find($id);

        return view('page.master.gudang.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = Gudang::find($id)
            ->update($request->except('_token', '_method', 'proengsoft_jsvalidation'));
        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('gudang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Gudang::destroy($id);

        return $data;
    }
}
