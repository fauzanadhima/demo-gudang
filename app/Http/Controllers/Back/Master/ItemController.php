<?php

namespace App\Http\Controllers\Back\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Master\Item\CreateRequest;
use App\Http\Requests\Master\Item\UpdateRequest;
use App\Models\Gudang;
use App\Models\Item;
use App\Models\kategori;
use App\Models\Satuan;
use Illuminate\Http\Request;
use DataTables;
use Session;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Carbon\Carbon;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {

            $data = Item::with(['gudang', 'kategori', 'satuan'])->select('*');

            $data = Item::with(['gudang', 'kategori'])->select('*');
            if($request->has('users')){
                $data->where('users_id', '=', auth()->user()->id);
            }
            if($request->has('users_id')){
                $data->where('users_id', '=', $request->users_id);
            }
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($a) {
                    return '<a href="' . route('item.edit', $a->id) . '" class="primary edit mr-1"><i class="fa fa-pencil mr-2"></i></a>
                            <a href="' . route('item.show', $a->id) . '" class="primary edit mr-1"><i class="fa fa-eye mr-2"></i></a>
                            <a href="' . route('item.destroy', $a->id) . '" class="danger delete delete-data-table mr-1" ><i class="fa fa-trash mr-2"></i></a>';
                })->addColumn('action2', function ($a) {
                    return '<a href="' . route('asset-user-unassign.index', ['item_id' => $a->id]) . '" class="danger delete delete-data-table mr-1" ><i class="fa fa-trash mr-2"></i></a>';
                })->addColumn('depresiasi', function ($a) {
                    $startDate = Carbon::parse($a->tanggal_pengadaan);
                    $endDate = Carbon::parse(date('Y-m-d'));
                    $diff = $startDate->diffInYears($endDate);

                    $persen = 100 / $a->estimasi_pakai;
                    $nilai= $a->nilai_barang * $persen / 100;
                    return $nilai * $diff;
                })->addColumn('nilai_akhir', function ($a) {
                    $startDate = Carbon::parse($a->tanggal_pengadaan);
                    $endDate = Carbon::parse(date('Y-m-d'));
                    $diff = $startDate->diffInYears($endDate);

                    $persen = 100 / $a->estimasi_pakai;
                    $nilai= $a->nilai_barang * $persen / 100;
                    return $a->nilai_barang -  ($nilai * $diff);
                })
                ->addColumn('qr', function ($a) {
                    return QrCode::generate($a->kode);;
                })
                ->rawColumns(['action', 'qr', 'action2'])
                ->make(true);
        }

        return view('page.master.item.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gudang = Gudang::pluck('name', 'id');
        $kategori = kategori::pluck('name', 'id');
        $satuan = Satuan::pluck('name', 'id');

        return view('page.master.item.create', compact('gudang', 'kategori', 'satuan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $data = Item::create(
           $request->all()
        );

        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('item.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Item::with(['gudang', 'kategori', 'satuan'])->find($id);


        return view('page.master.item.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Item::find($id);
        $gudang = Gudang::pluck('name', 'id');
        $kategori = kategori::pluck('name', 'id');
        $satuan = Satuan::pluck('name', 'id');

        return view('page.master.item.edit', compact('data','gudang', 'kategori', 'satuan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = Item::find($id)
            ->update($request->except('_token', '_method', 'proengsoft_jsvalidation'));
        if($data){
            Session::flash('keterangan', 'Data berhasil di simpan');
        }

        return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Item::destroy($id);

        return $data;
    }
}
