<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;

class UserItemController extends Controller
{
    public function index(Request $request)
    {

        return view('page.user-item.index');
    }
    public function assign(Request $request)
    {
        $item = Item::where('users_id', null)->pluck('name', 'id');
        $user = User::pluck('name', 'id');

        return view('page.user-item.assign', compact('item', 'user'));
    }


    public function assignItem(Request $request)
    {
//        return $request->all();
        Item::find($request->item_id)->update(
            [
                'users_id' => $request->user_id
            ]);

        return redirect()->back();

    }
    public function unAssignItem(Request $request)
    {
        Item::find($request->item_id)->update(
            ['users_id' => null
        ]);

//        return redirect()->back();

    }
}
