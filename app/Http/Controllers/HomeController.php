<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nilaiAwal = Item::sum('nilai_barang');

        $nilaiAkhir = Item::count('id');

//        return $data;

        return view('home',compact('nilaiAwal', 'nilaiAkhir'));
    }
}
