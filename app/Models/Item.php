<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $guarded = ['proengsoft_jsvalidation'];

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }
    public function kategori()
    {
        return $this->belongsTo(kategori::class, 'kategori_id');
    }
    public function satuan()
    {
        return $this->belongsTo(Satuan::class, 'satuan_id');
    }
}
